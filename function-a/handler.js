'use strict'
const axios = require('axios');

module.exports = async (event, context) => {
  const result = {
    'body': JSON.stringify(event.body),
    'content-type': event.headers["content-type"]
  }

  var bdy = "hello " + JSON.stringify(event.body);
  await axios.post('http://127.0.0.1:8080/function/function-b', bdy);


  return context
    .status(200)
    .succeed(result)
}


